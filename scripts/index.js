// Теоретичні питання

// 1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval().

//          setTimeout дозволяє викликати функцію один раз через певний проміжок часу. setInterval дозволяє викликати функцію регулярно, повторюючи виклик через певний проміжок часу.

// 2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?

//          Спрацює після виконання написаного вище коду

// 3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?

//          Для очищення памяті додатку, оскільки setTimeout() та setInterval() залишаються в памяті.

const stop = document.querySelector('.stop');
const go = document.querySelector('.go');
const imageShow = document.querySelectorAll('.image-to-show');

let COUNT = 1;
let IMGCOUNT = 0;
let lent = imageShow.length;
let nIntervId;

const slader = () => {
    nIntervId = setInterval(() => {
        if (COUNT % 3 === 0) {
            if (IMGCOUNT === lent - 1) {
                imageShow[IMGCOUNT].classList.toggle('diplayon');
                IMGCOUNT = 0;
                imageShow[IMGCOUNT].classList.toggle('diplayon');
            } else {
                imageShow[IMGCOUNT].classList.toggle('diplayon');
                imageShow[IMGCOUNT + 1]?.classList.toggle('diplayon');

                IMGCOUNT++;
            }
        }

        COUNT++;
    }, 1000);
};

slader();

stop.addEventListener('click', e => {
    clearInterval(nIntervId);
});

go.addEventListener('click', e => {
    slader();
});
